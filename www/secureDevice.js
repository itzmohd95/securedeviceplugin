var exec = require('cordova/exec');

function SecureDevice () {	
    this.startSecureDevice = function() {
		exec(function(success) { console.log("SecureDevice::init Success: " + success) }, function(error) { console.log("SecureDevice::init Error: " + error) }, "SecureDeviceSDK", "SecureDevice", []);
	};
	/*this.startSecureDevice = function(success, error) {
		exec(success, error, "SecureDeviceSDK", "SecureDevice", []);
	};*/
}

module.exports = new SecureDevice();